<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<?php global $base_url;?>
<title><?php print $head_title ?></title>
<?php print $head ?>
<?php print $styles ?>
<?php print $scripts; ?>

<script src="misc/jquery.js" type="text/javascript"></script>
<script src="<?php print drupal_get_path('theme', 'bluemasters') . '/js/bluemasters.js'?>" type="text/javascript"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/swfobject/2.1/swfobject.js" type="text/javascript"></script>

</head>
<body>

<div id="page">

<div id="header-top"><!--header-top-->
    <div id="header-top-inside" class="clearfix">
        <div id="header-top-inside-left">
	  <div id="header-top-inside-left-content">
	    <a href="http://apps.facebook.com/intelinnovators/"><img src="/sites/all/themes/bluemasters/images/facebook-logo.png"></a>
	    <a href="http://www.youtube.com/watch?v=rv2i1WH1ArU"><img src="/sites/all/themes/bluemasters/images/youtube-logo.png"></a>
<!--	    <a href="#"><img src="/sites/all/themes/bluemasters/images/kickstarter-logo.png"></a>
	    <a href="#"><img src="/sites/all/themes/bluemasters/images/twitter-logo.png"></a>
	    <a href="#"><img src="/sites/all/themes/bluemasters/images/sourceforge-logo.png"></a> -->
	  </div>
	</div>
        <div id="header-top-inside-left-feed"><?php print $feed_icons ?></div>
        <div id="header-top-inside-right">	    <div id="navigation">
	    	<?php //if (isset($primary_links)) { ?><?php //print theme('links', $primary_links, array('class' =>'links', 'id' => 'primary-links')) ?><?php //} ?>
	        <?php print menu_tree($menu_name = 'primary-links'); ?>
	    </div><!--navigation--></div>  
    </div>
</div><!--/header-top-->

<div> </div>

<div id="wrapper">

	<div id="header" class="clearfix">
	
	    <div id="logo"> 
			<?php
	        // Prepare header
	        $site_fields = array();
	        if ($site_name) {
	        	$site_fields[] = check_plain($site_name);
	        }
	        if ($site_slogan) {
	        	$site_fields[] = check_plain($site_slogan);
	        }
	        $site_title = implode(' ', $site_fields);
	        if ($site_fields) {
	        	$site_fields[0] = '<span>'. $site_fields[0] .'</span>';
	        }
	        $site_html = implode(' ', $site_fields);
	        
	        if ($logo || $site_title) {
	        	print '<a href="'. check_url($front_page) .'" title="'. $site_title .'">';
	        if ($logo) {
	        	print '<img src="'. check_url($logo) .'" alt="'. $site_title .'" id="logo-image" />';
	        }
	        print '<div style="display:none">'.$site_html .'</div></a>';
	        }
	        ?>
	    </div> <!--logo-->
	    

	
	</div><!--header-->


	<div id="banner" class="clearfix">
		<?php //print $banner;?>
		<div class="main_view">
			<div class="window">
				<div class="image_reel">
					
					<a href="/Mission"><img src="/sites/all/themes/bluemasters/images/slide2.jpg"></a>
					<a href="/Tactile%20Screen"><img src="/sites/all/themes/bluemasters/images/slide1.jpg"></a>
					<a href="/Tactile%20Screen"><img src="/sites/all/themes/bluemasters/images/slide13.jpg"></a>
					<a href="/Proximity%20Sensor"><img src="/sites/all/themes/bluemasters/images/slide12.jpg"></a>
					<a href="/Proximity%20Sensor"><img src="/sites/all/themes/bluemasters/images/slide10.jpg"></a>
					<a href="Wireless%20Beacons"><img src="/sites/all/themes/bluemasters/images/slide11.jpg"></a>
					<a href="/Fuel%20Cell%20Battery"><img src="/sites/all/themes/bluemasters/images/slide14.jpg"></a>
					<a href="/Tactile%20Screen"><img src="/sites/all/themes/bluemasters/images/slide15.jpg"></a>
					
				</div>
				<div class="descriptions">
					<div class="desc" style="display: none;"></div>
					<div class="desc" style="display: none;"></div>
					<div class="desc" style="display: none;"></div>
					<div class="desc" style="display: none;"></div>
					<div class="desc" style="display: none;"></div>
					<div class="desc" style="display: none;"></div>
					<div class="desc" style="display: none;"></div>
					<div class="desc" style="display: none;"></div>
				</div>
				
			</div>
		
			<div class="paging" style="display: block;">
				<a rel="1" href="#" class="">1</a>
				<a rel="2" href="#" class="">2</a>
				<a rel="3" href="#" class="">3</a>
				<a rel="4" href="#" class="">4</a>
				<a rel="5" href="#" class="">5</a>
				<a rel="6" href="#" class="">6</a>
				<a rel="7" href="#" class="">7</a>
				<a rel="8" href="#" class="">8</a>
			</div>
		</div>
		
	</div><!--banner-->
	
	<div id="slide-navigation"></div>

	<div id="home-blocks-area" class="clearfix">
		<a href="Wireless%20Beacons"><div id="home-block-1" class="home-block">
	    	<?php print $home_area_1;?> 		
	    </div></a>
	   <a href="/Tactile%20Screen"> <div id="home-block-2" class="home-block">
	    	<?php print $home_area_2;?> 
	    </div> </a>
	    <a href="/Proximity%20Sensor"><div id="home-block-3" class="home-block">
	    	<?php print $home_area_3;?>
	    	<div id="home-block-3-b">
	    		<?php print $home_area_3_b;?> 
	    	</div></a>
	    </div>
	</div>    
	
	<?php 
	// uncomment this to get news feed in your home page
	// you have to take care about the look'n'feel 
	// print $content;
	?> 
     
</div><!-- /wrapper-->

<div id="footer">
    <div id="footer-inside" class="clearfix">
    	<div id="footer-left">
    		<div id="footer-left-1">
    			<?php print $footer_left_1;?>
    		</div>
    		<div id="footer-left-2">
    			<?php print $footer_left_2;?>
    		</div>
        </div>
        <div id="footer-center">
        	<?php print $footer_center;?>
        </div>
        <div id="footer-right">
        	<?php print $footer_right;?>
        </div>
    </div>
</div>

<div id="footer-bottom">
    <div id="footer-bottom-inside" class="clearfix">
    	<div style="float:left">
    		<?php print $footer_message;?> 
    	</div>
    	<div style="float:right">
	        <?php if (isset($secondary_links)) : ?>
	          <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
	        <?php endif; ?>      	
    	</div>

</div>
    
<?php print $closure ?>


</div><!-- /page-->

</body>
</html>
